<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

// load structure library if it exists
if( file_exists(PATH_THIRD.'structure/sql.structure.php'))
	require_once PATH_THIRD.'structure/sql.structure.php';

require_once PATH_THIRD . 'transcribe/libraries/Cache.php';

class Transcribe {

	private $EE;
	private $site_id;

	public $routes = array();
	public $return_data = NULL;

	public function __construct()
	{
		$this->EE =& get_instance();
		$this->site_id = $this->EE->config->config['site_id'];
	}

	/**
	 * {exp:transcribe:translate} tag pair
	 *
	 * Parameters:
	 *	id - the variable id to use
	 *	name - the variable name to use
	 *
	 * Replaces content between the tags with the translated content variable of
	 * the current langauge with the id or name specified.
	 */
	public function translate()
	{
		// call the replace function
		return $this->replace();
	}

	/**
	 * {exp:transcribe:replace} single tag
	 *
	 * Parameters:
	 *	id - the variable id to use
	 *	name - the variable name to use
	 *
	 * Outputs the translated content variable of the current language with the
	 * id or name specified.
	 */
	public function replace()
	{
		if( empty($_SESSION['transcribe']['id']) )
		{
			$lang_id = $this->_set_current_language();
			$lang_id = $lang_id['id'];
		}
		else
		{
			$lang_id = $_SESSION['transcribe']['id'];
		}
		// determine variable through id or name
		$variable_id = $this->EE->TMPL->fetch_param('id');
		$variable_name = strtolower($this->EE->TMPL->fetch_param('name'));
		
		// the following params will be used to pull a variable from a specific site
		$site_name = $this->EE->TMPL->fetch_param('site');
		// language to be passed in as a language abbreviation
		$var_lang =  $this->EE->TMPL->fetch_param('lang');
		
		//get the site id fom the name
		if( !empty($site_name) )
		{
			$site_id = $this->EE->db->select('site_id')->get_where('sites', array('site_name' => $site_name), 1)->row();
			
			if( !empty($site_id->site_id) )
				$this->site_id = $site_id->site_id;
		}
		
		if( !empty($var_lang) )
		{
			$var_lang_id = $this->EE->db->select('id')->get_where('transcribe_languages', array('abbreviation'=> $var_lang), 1)->row();
			
			if( !empty($var_lang_id->id) )
				$lang_id = $var_lang_id->id;
		}
		
		// check to see if we have the variables cached already
		$all_vars_for_lang = Cache::get(array('variables',$this->site_id, $lang_id));
		
		if( empty($all_vars_for_lang) )
		{
			// not cached, grab all vars for the language being queried
			$this->EE->db->select('transcribe_variables.name, transcribe_translations.content, transcribe_variables.id');
			$this->EE->db->join('transcribe_variables', 'transcribe_variables.id = transcribe_variables_languages.variable_id');
			$this->EE->db->join('transcribe_translations', 'transcribe_translations.id = transcribe_variables_languages.translation_id');
			$this->EE->db->where('transcribe_variables_languages.language_id', $lang_id);
			$this->EE->db->where('transcribe_variables_languages.site_id', $this->site_id);
			$variables = $this->EE->db->get('transcribe_variables_languages');
			$variables = $variables->result_array();
			
			// var name is key since this is generally how people use variables
			foreach($variables as $var)
				$all_vars_for_lang[strtolower($var['name'])] = $var;

			Cache::set(array('variables', $this->site_id, $lang_id), $all_vars_for_lang);
		}
		
		// were going to set the contet of the tag pair as the variable_name if the var_id and var_name are empty
		if( empty($variable_id) && empty($variable_name))
			$variable_name = $this->EE->TMPL->tagdata;
		
		if( !empty($variable_id) )
		{
			foreach( $all_vars_for_lang  as $var_name => $var )
				if( $var['id'] == $variable_id )
					$variable = $var['content'];
		}
		elseif( !empty($variable_name) )
		{
			if( !empty($all_vars_for_lang[$variable_name]['content']) )
				$variable = $all_vars_for_lang[$variable_name]['content'];
		}
	
		if( !empty($variable) )
		{
			$this->return_data = empty($variable) ? $this->EE->TMPL->tagdata : $variable;
		}
		else
		{
			$this->return_data = empty($this->EE->TMPL->tagdata) ? 'Transcribe: Translation not found.' : $this->EE->TMPL->tagdata;
		}

		return $this->return_data;
	}

	/**
	 * {exp:transcribe:uri} single tag
	 *
	 * Parameters:
	 *  id - id of the language to use
	 *  name - name of the language to use
	 *
	 *  Sets the language to be used from the template. This can be used to
	 *  override the default behaviour of Transcribe which is to figure out
	 *  what language to load automatically.
	 */
	public function language()
	{
		$old_language = $_SESSION['transcribe'];

		// determine language through id or name
		$language_id = $this->EE->TMPL->fetch_param('id');
		$language_name = $this->EE->TMPL->fetch_param('name');

		if( !empty($language_id) )
			$this->EE->db->or_where('id', $language_id);

		if( !empty($language_name) )
			$this->EE->db->or_where('name', $language_name);

		$language = $this->EE->db->get('transcribe_languages', 1);
		$language = $language->row();

		if( !empty($language) )
		{
			$this->_set_current_language($language->abbreviation);

			if(!empty($old_language['abbreviation']) AND ($old_language['abbreviation'] != $language->abbreviation))
			{
				$segments = explode('/', $this->EE->session->tracker[0]);

				$template_segments = array_slice($segments, 0, 2);
				$extra_segments = array_slice($segments, 2);

				$new_url  = $this->EE->functions->fetch_site_index(TRUE);
				$new_url .= $this->_uri_reverse_lookup( implode('/', $template_segments) );
				$new_url .= '/' . implode('/', $extra_segments);

				$new_url = $this->EE->functions->remove_double_slashes($new_url);

				// send back to last page
				$this->EE->functions->redirect($new_url);
			}
		}
	}

	/**
	 * {exp:transcribe:language_abbreviation} single tag
	 *
	 * Parameters:
	 *	nonet
	 *
	 * Outputs the current languages abbreviation
	 */
	public function language_abbreviation()
	{
		if(!empty($_SESSION['transcribe']['abbreviation']))
			return $_SESSION['transcribe']['abbreviation'];
	}

	/**
	 * {exp:transcribe:uri} single tag
	 *
	 * Parameters:
	 *	path - the path to output
	 *
	 * Outputs the path specified in it's translated state for the current language.
	 * Used as a replacement to {path=""}
	 */
	public function uri()
	{
		$site_name = $this->EE->TMPL->fetch_param('site');
		// language to be passed in as a language abbreviation
		$uri_lang =  $this->EE->TMPL->fetch_param('lang');
		
		$path = $this->EE->TMPL->fetch_param('path');
		
		//get the site id fom the name
		if( !empty($site_name) )
		{
			$site_data= $this->EE->db->get_where('sites', array('site_name' => $site_name), 1)->row();

			if( !empty($site_data->site_id) )
				$this->site_id = $site_data->site_id;
		}
		
		// get the language id from the abbreviation
		if( !empty($uri_lang) )
		{
			$uri_lang_data = $this->EE->db->get_where('transcribe_languages', array('abbreviation'=> $uri_lang), 1)->row();

			if( !empty($uri_lang_data->id) )
			{
				$lang_id = $uri_lang_data->id;
				$_SESSION['transcribe']['uri_override'] = $lang_id;
			}
		}
		
		// remove the current site base if it's present
		$path = str_replace($this->EE->functions->fetch_site_index(TRUE), '', $path);
		$path = $this->_uri_reverse_lookup($path);

		// pull vars for categories
		$category_id = $this->EE->TMPL->fetch_param('category_id');
		$category_prefix = $this->EE->TMPL->fetch_param('category_prefix');
		$category_url_indicator = $this->EE->TMPL->fetch_param('category_url_indicator');
		$category_url_title = $this->EE->TMPL->fetch_param('category_url_title');

		if(empty($category_prefix))
		{
			$category_prefix = 'C';
		}

		if( $category_url_indicator == 'yes' )
		{
			// pull the category url indicator here
			$this->EE->db->select('site_channel_preferences');
			$data = $this->EE->db->get('sites');
			$data = $data->row();
			$data = unserialize(base64_decode($data->site_channel_preferences));

			// need to cache this so we don't have a query each time.
			$category_reserved_word = $data['reserved_category_word'];
		}

		// get site settings
		$settings = $this->_get_settings();
		$this->EE->config->set_item('site_index', $this->_set_base($this->EE->config->config['site_index']));

		// build category URL's
		if(!empty($category_reserved_word))
		{
			$path .= '/'.$category_reserved_word;
		}
		if(!empty($category_url_title))
		{
			$path .= '/'.$category_url_title;
		}
		if( !empty($category_id) && !empty($category_prefix) )
		{
			$path .= '/'.$category_prefix.$category_id;
		}
		if( empty($site_name) )
			$site_index = $this->EE->functions->fetch_site_index(1);
		else
		{
			$site_system_preferences = unserialize(base64_decode($site_data->site_system_preferences));
			$site_index = $site_system_preferences['site_url'];
		}
		
		if( !empty($uri_lang) && $uri_lang_data->force_prefix == 1 )
			$site_index = $site_index .= $uri_lang.'/';

		
		$url = $site_index . $path . '/';
		return $this->EE->functions->remove_double_slashes($url);
	}

	/**
	 * {exp:transcribe:language_links} tag pair
	 *
	 * Retrieves an array of availabled languages for the current site. Can be
	 * used in the template to create a language switcher.
	 */
	public function language_links()
	{
		$variables = array();

		// get languages that contain translations (variables or entries)
		$this->EE->db->select('tl.*');
		$this->EE->db->from('transcribe_languages AS tl');
		$this->EE->db->join('transcribe_variables_languages AS tvl', 'tvl.language_id = tl.id AND tvl.site_id = '.$this->site_id, 'LEFT');
		$this->EE->db->join('transcribe_entries_languages AS tel', 'tel.language_id = tl.id', 'LEFT');
		$this->EE->db->join('channel_titles AS ct', 'ct.entry_id = tel.entry_id AND ct.site_id = '.$this->site_id, 'LEFT');
		$this->EE->db->or_where('tvl.site_id IS NOT NULL');
		$this->EE->db->or_where('ct.site_id IS NOT NULL');
		$this->EE->db->group_by('tl.id');
		$languages = $this->EE->db->get();
		$languages = $languages->result_array();
		
		// the following allows you to make links for only languages that have relationships to the current entry.
		
		if(empty($languages)) return FALSE;

		
		// grab all related entries
		$entry_id = $this->EE->TMPL->fetch_param('entry_id');
		$has_entry = $this->EE->TMPL->fetch_param('has_entry');
		
		if(!empty($entry_id) && !empty($has_entry))
		{
			// grab the relationship
			$relationship =$this->EE->db
				->select('relationship_id')
				->from('transcribe_entries_languages')
				->where("entry_id", $entry_id)
				->get();
		
			$relationship = $relationship->row('relationship_id');
			
			// what langauges have a translation?
			$entries = $this->EE->db
				->select('language_id')
				->from('transcribe_entries_languages')
				->where('relationship_id ', $relationship)
				->get();
			$entries = $entries->result_array();
			
			// remove languages that don't have a translation
			foreach($languages as $key => $language)
			{
				$pres = 0;

				foreach($entries as $entry)
					if($entry['language_id'] == $language['id'])
						$pres = 1;
				
				if($pres == 0)
				{
					unset($languages[$key]);
				}
			}
		}
		
		// need to make sue languages isn't empty again after we removed the ones that don't have entries.
		if(empty($languages)) return FALSE;
		
		// get action url
		$trigger_url = $this->action_url();

		// get currently selected language
		$current_language = $this->_set_current_language();

		foreach( $languages as $language )
		{
			$variables['languages'][] = array(
				'id' => $language['id'],
				'name' => $language['name'],
				'abbreviation' => $language['abbreviation'],
				'link' => $trigger_url.AMP.'lang='.$language['abbreviation'],
				'current' => ($current_language['id'] == $language['id']) ? TRUE : FALSE,
			);
		}

		return $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, array($variables));
	}

	/**
	 * Accepts the action of switching the current language from the switcher
	 * and redirects to the properly translated url of the current page (where
	 * the langauge was switched).
	 */
	public function language_switcher()
	{
		$this->EE->functions->cached_index = NULL;
		$_SESSION['transcribe'] = $this->language = NULL;

		// set new language
		$language = $this->EE->input->get('lang');
		$language = empty($language) ? $this->EE->input->post('lang') : $language;
		
		$this->_set_current_language($language);
		
		if( !empty($_SESSION['transcribe_no_match']) )
		{
			foreach($this->EE->session->tracker as $raw_route)
				$tracker_routes[] = trim($raw_route, '/');

			foreach($_SESSION['transcribe_no_match'] as $key => $no_match_route)
				$_SESSION['transcribe_no_match'][$key] =  trim($no_match_route, '/');

			$diff = array_diff($tracker_routes, $_SESSION['transcribe_no_match'] );

			if( !empty($diff) )
			{
				$diff = array_values($diff);
				$uri_string = $diff[0];
			}

		}
		if( !empty($_SESSION['transcribe_no_match_segment']) )
		{
			//transcribe_no_match_segment
			if( empty($diff) )
			{
				foreach($this->EE->session->tracker as $raw_route)
				{
					$raw_route = '/'.ltrim($raw_route, '/');
					$tracker_routes[] = rtrim($raw_route, '/').'/';
				}
				
			}
			else
			{
				foreach($diff as $raw_route)
				{
					$raw_route = '/'.ltrim($raw_route, '/');
					$tracker_routes[] = rtrim($raw_route, '/').'/';
				}
			}
			
			foreach($_SESSION['transcribe_no_match_segment'] as $no_match_segment)
			{
				$no_match_segment = '/'.ltrim($no_match_segment, '/');
				$no_match_segment = rtrim($no_match_segment, '/').'/';

				$matches = $this->array_contains($tracker_routes, $no_match_segment);
				$tracker_routes = array_diff($tracker_routes, $matches);
			}
			//we now have a clean tracker routes array.
			if( !empty($tracker_routes) )
			{
				$tracker_routes = array_values($tracker_routes);
				$uri_string = $tracker_routes[0];
			}
		}

		if( empty($uri_string) )
		{
			$uri_string = isset($this->EE->session->tracker[0]) ? $this->EE->session->tracker[0] : (isset($_SESSION['HTTP_REFERER']) ? $_SESSION['HTTP_REFERER'] : 'index');
		}

		if( $uri_string == 'index' )
			$uri_string = '/';
			
			
		$segments = explode('/', $uri_string);

		$is_abbreviation = $this->EE->db->get_where('transcribe_languages', array('abbreviation' => $segments[0]))->row();
	
		if( !empty($is_abbreviation) )
				array_shift($segments);

		// Eventually move the below logic over to the uri_reverse_lookup funciton (to support cats as well etc)
		
		//remove empty segments & reindex array
		$segments = array_values(array_filter($segments));
		
		// check if a template exists for segment_1/segment_2/segment_3
		if( !empty($segments) AND !empty($segments[1]) AND !empty($segments[2]))
		{
			$route = $this->_uri_reverse_lookup( implode('/', array($segments[0], $segments[1], $segments[2])), 1);
			
			if( !empty($route) )
				$segments = array_slice($segments, 3);
		}

		// check if a template exists for segment_1/segment_2
		if( !empty($segments) AND !empty($segments[1]) )
		{
			$route = $this->_uri_reverse_lookup( implode('/', array($segments[0], $segments[1])), 1);

			if( !empty($route) )
				$segments = array_slice($segments, 2);
		}

		// check if a template exists for segment_1
		if( empty($route) AND !empty($segments) )
		{
			$route = $this->_uri_reverse_lookup($segments[0], 1);

			if( !empty($route) )
				$segments = array_slice($segments, 1);
		}

		// check if the remaining segments are an entry, category or search.
		// we might need to remove the category check here
		
		if( !empty($route) AND !empty($segments) )
		{
			$has_entry = FALSE;
			$is_cat = FALSE;

			// check structure segements here
			foreach($segments as $segment)
			{
				// check if segment is an entry
				$this->EE->db->from('channel_titles as ct');
				$this->EE->db->join('transcribe_entries_languages as tel', 'tel.entry_id = ct.entry_id');
				$this->EE->db->where(array('url_title' => $segment, 'tel.language_id' => $this->language['id']));
				$is_entry = $this->EE->db->get()->row();
				if( ! empty($is_entry) ) $has_entry = TRUE;

				// check if segment is a search id
				if( !empty($this->EE->TMPL->module_data['Search']) )
					$is_search = $this->EE->db->get_where('search', array('search_id' => $segment), 1)->row();
				
				if( ! empty($is_search) ) $has_entry = TRUE;

				// check if segment is a category ID or Category url
				// $prefix_removed is the segment with the category prefix removed
				$prefix_removed = str_replace('C', '', $segment);
				if( !empty($prefix_removed) )
				{
					if(is_numeric($prefix_removed))
					{
						// this means we have a category id
						$is_cat_id = $this->EE->db->get_where('categories', array('cat_id' => $prefix_removed))->row();
						if( !empty($is_cat_id) ) $is_cat = TRUE;
					}
					else
					{
						//checking to see if it's a category URL segment
						$is_cat_url = $this->EE->db->get_where('categories', array('cat_url_title' => $segment))->row();
						if( !empty($is_cat_url) ) $is_cat = TRUE;
					}
				}
			}

			// send to site index
			if( !$has_entry AND !$is_cat )
				$this->EE->functions->redirect($this->EE->functions->fetch_site_index(TRUE));
		}

		if(empty($route))
			$route = '';

		$new_url = $this->EE->functions->remove_double_slashes( $this->EE->functions->fetch_site_index(TRUE) . '/' . $route . '/' . implode('/', $segments) );

		// send back to last page
		$this->EE->functions->redirect($new_url);
	}
	
	/*
	 * This function is used to generate the entry ids for the current language in a channel
	 * with the channel name passed in from the teamplate
	 * orignally built for use with the next_prev tags
	 */
	public function entry_ids()
	{
		$language_id = $this->_set_current_language();
		$language_id = $language_id['id'];
		
		$channel_name = $this->EE->TMPL->fetch_param('channel');
		
		$channel_id = $this->EE->db->select('channel_id')
									->from('channels')
									->where(array('channel_name'=> $channel_name, 'site_id' => $this->site_id))->get()->row('channel_id');
		
		$entry_ids = $this->EE->db->select('ct.entry_id')
									->from('channel_titles as ct')
									->join('transcribe_entries_languages as tel', 'tel.entry_id = ct.entry_id')
									->where(array('ct.channel_id'=> $channel_id, 'tel.language_id' => $language_id, 'ct.site_id' => $this->site_id))->get();
		if($entry_ids->num_rows() > 0)
			foreach($entry_ids->result() as $row)
				$return_data[] = $row->entry_id;

		return implode('|', $return_data);
	}
	

	/**
	 * This function sets routes that can't be switched between languages
	 */
	public function no_match()
	{
		$route = $this->EE->TMPL->fetch_param('url');
		$segment = $this->EE->TMPL->fetch_param('segment');
		$key = "";

		if( !empty($route))
		{
			if( !empty($_SESSION['transcribe_no_match']) )
				$key = array_search($route, $_SESSION['transcribe_no_match']);

			if(!is_int($key))
				$_SESSION['transcribe_no_match'][] = $route;
		}
		if( !empty($segment) )
		{
			if( !empty($_SESSION['transcribe_no_match_segment']) )
				$key = array_search($segment, $_SESSION['transcribe_no_match_segment']);

			if(!is_int($key))
				$_SESSION['transcribe_no_match_segment'][] = $segment;
		}
	}

	/**
	 * Retrieves the action url for this module
	 */
	public function action_url()
	{
		$trigger_url = $this->EE->functions->fetch_site_index(0);

		$action_id = $this->EE->db->get_where('actions', array( 'class'=>'Transcribe', 'method'=>'language_switcher' ), 1);
		$action_id = $action_id->row('action_id');

		// remove any current abbreviations
		$trigger_url = $this->_remove_abbreviation($trigger_url);

		if( strpos($trigger_url, SELF) === FALSE )
			$trigger_url .= '/' . SELF;

		$trigger_url .= QUERY_MARKER . 'ACT=' . $action_id;

		return $this->EE->functions->remove_double_slashes($trigger_url);
	}

	/**
	 * Load the default template routes and the translated versions for the
	 * current language.
	 */
	private function _load_template_routes()
	{
		if( empty($this->template_routes) )
		{
			$this->template_routes = array();

			$this->EE->db->select('tl.id as language_id, tl.name as language');
			$this->EE->db->select('tg.group_name as default_group, t.template_name as default_template');
			$this->EE->db->select('ttgl.content as transcribe_group, ttl.content as transcribe_template');
			$this->EE->db->from('template_groups tg');
			$this->EE->db->join('templates t', 'tg.group_id = t.group_id');
			$this->EE->db->join('transcribe_template_groups_languages ttgl', 'ttgl.template_group_id = tg.group_id');
			$this->EE->db->join('transcribe_templates_languages ttl', 'ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id');
			$this->EE->db->join('transcribe_languages tl', 'tl.id = ttgl.language_id');
			$this->EE->db->where('tg.site_id', $this->site_id);
			$this->EE->db->order_by('tg.group_name, t.template_name');
			$routes_result = $this->EE->db->get();
			$routes_result = $routes_result->result();

			foreach( $routes_result as $route )
			{
				$this->template_routes[$route->default_group.'/'.$route->default_template][$route->language_id] = $route->transcribe_group.'/'.$route->transcribe_template;
			}
		}

		return $this->template_routes;
	}

	/**
	 * set the language for this request
	 *
	 * Sets the language for the current request. If an abbreviation is not
	 * passed, we check the session for the language and finally we find the
	 * default language for the site if the previous options have been
	 * exhausted.
	 *
	 * $param string $abbreviation (optional) abbreviation of language to set
	 * @return array $language
	 */
	public function _set_current_language( $abbreviation='' )
	{
		// start session if it hasn't been already
		if( session_id() == '' ) session_start();
		// if cookie exists, restore into session
		$cookie = $this->_get_cookie();
		if( !empty($cookie) AND empty($_SESSION['transcribe']) ) $_SESSION['transcribe'] = $cookie;

		// get settings
		$settings = $this->_get_settings();

		if( empty($this->language) OR ($abbreviation != $this->language['abbreviation']) )
		{
			$language = NULL;

			if( !empty($abbreviation) )
			{
				$language = Cache::get(array('current_lang', $abbreviation));
				$this->language = is_object($language) ? get_object_vars($language) : array(); 
				if(empty($language))
				{
					$language = $this->EE->db->get_where('transcribe_languages', array('abbreviation' => $abbreviation));
					$language = $language->row();
					$this->language = is_object($language) ? get_object_vars($language) : array(); 
					Cache::set(array('current_lang', $abbreviation), $language);
				}
			}

			if( empty($language) AND !empty($_SESSION['transcribe']) )
			{
				$this->language = $language = $_SESSION['transcribe'];

				$language_check = Cache::get(array('current_lang', $_SESSION['transcribe']['abbreviation']));
				
				if(empty($language_check))
					$language_check = $this->EE->db->get_where('transcribe_languages', array('abbreviation' => $_SESSION['transcribe']['abbreviation']), 1)->row();

				if( empty($language_check) )
					unset($this->language, $language, $_SESSION['transcribe']);
				
			}

			if( empty($language) AND empty($_SESSION['transcribe']) )
			{
				$language_id = empty($settings->language_id) ? 1 : $settings->language_id;

				$language = $this->EE->db->get_where('transcribe_languages', array('id' => $language_id));
				$language = $language->row_array();
				$this->language = $language;
			}

			if( !empty($settings->force_prefix) )
			{
				if( $settings->force_prefix == 1 || ($settings->force_prefix == 2 && !empty($this->language['force_prefix'])) )
				{
					// remove SELF constant from site_index (if found). re-add it later if it exists.
					$index_has_self = FALSE;
					if( strpos($this->EE->config->config['site_index'], SELF) !== FALSE )
					{
						$index_has_self = TRUE;
						$this->EE->config->set_item('site_index', str_replace(SELF, '', $this->EE->config->config['site_index']));
					}

					// remove any current abbreviations
					$this->EE->config->config['site_index'] = $this->_remove_abbreviation($this->EE->config->config['site_index']);

					$site_index = ($index_has_self ? SELF . '/' : '') . $this->language['abbreviation'] . '/';
					$this->EE->config->set_item('site_index', $this->EE->functions->remove_double_slashes($site_index));
				}
				else
				{
					$this->EE->config->config['site_index'] = $this->_remove_abbreviation($this->EE->config->config['site_index']);
				}
			}
			else
			{
				$this->EE->config->config['site_index'] = $this->_remove_abbreviation($this->EE->config->config['site_index']);
			}
		}
		
		if( !empty($this->language['id']) && !empty($cookie['id']) && $cookie['id'] != $this->language['id'])
			$this->_save_cookie($this->language);
		
		return $_SESSION['transcribe'] = $this->language;
	}

	/**
	 * Removes a potential language abbreviation from a string.
	 *
	 * Retrieve a list of all possible abbreviations from the database and
	 * remove the first occurence in the string provided. Return the altered
	 * string
	 *
	 * $param string $string string to remove abbreviation
	 * @return string $string modified string
	 */
	public function _remove_abbreviation( $string )
	{
		$abbreviations_result = Cache::get(array('abbreviations'));

		if(empty($abbreviations_result))
		{
			$abbreviations_result = $this->EE->db->select('abbreviation')->get('transcribe_languages')->result();
			Cache::set(array('abbreviations'), $abbreviations_result);
		}

		$abbreviations = array();
		foreach($abbreviations_result as $row)
			$abbreviations[] = $row->abbreviation;

		$pattern = '/(\/(?:' . implode('|', $abbreviations) . ')\/)/';
		return preg_replace($pattern, '', $string, 1);
	}

	/**
	 * retrieves the route/relationship
	 *
	 * Retrieves the template_group/template_name routes and their relationship
	 * (the actual template they point to) from the database.
	 *
	 * 1. Get transcribes routes
	 * 2. Get transcribes routes for the default template group
	 * 3. Get ExpressionEngine's normal routes
	 * 4. Get ExpressionEngine's routes for the default template group.
	 *
	 * @return array $routes
	 */
	public function _get_routes()
	{
		if( empty($this->routes) )
		{
			$sql = array();

			// Transcribe Routes
			$sql[] = "SELECT";
			$sql[] = "CONCAT_WS('/', ttgl.content, ttl.content) as route,";
			$sql[] = "CONCAT_WS('/', tg.group_name, t.template_name) as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;

			$sql[] = "UNION ALL";

			// ExpressionEngine Normal Routes
			$sql[] = "SELECT";
			$sql[] = "CONCAT_WS('/', tg.group_name, t.template_name) as route,";
			$sql[] = "CONCAT_WS('/', tg.group_name, t.template_name) as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;

			$sql[] = "UNION ALL";

			// ExpressionEngine Default Template Group Routes
			$sql[] = "SELECT";
			$sql[] = "t.template_name as route,";
			$sql[] = "t.template_name as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "WHERE tg.is_site_default = 'y'";
			$sql[] = "AND tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;

			$sql[] = "UNION ALL";

			// Transcribe Default Template Routes
			$sql[] = "SELECT";
			$sql[] = "ttl.content as route,";
			$sql[] = "t.template_name as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "WHERE tg.is_site_default = 'y'";
			$sql[] = "AND tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;

			$sql[] = "UNION ALL";

			$sql[] = "SELECT";
			$sql[] = "ttgl.content as route,";
			$sql[] = "tg.group_name as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "WHERE  t.template_name = 'index'";
			$sql[] = "AND tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id . "";

			$sql[] = "UNION ALL";

			$sql[] = "SELECT";
			$sql[] = "tg.group_name as route,";
			$sql[] = "tg.group_name as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "WHERE t.template_name = 'index'";
			$sql[] = "AND tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id . "";

			$routes = $this->EE->db->query(implode(' ', $sql));
			$this->routes = $routes->result();

			$routes->free_result();
		}

		return $this->routes;
	}

	/**
	 * URI Reverse Lookup to point urls to the properly translated url for the
	 * current langauge. Basically, take every combination of
	 * template_group/template_name from every langauge and figure out where it
	 * should be pointing based on the current langauge.
	 * Also supports related structure data
	 */
	public function _uri_reverse_lookup( $uri_string, $return = 0 )
	{
		$lang_id = $_SESSION['transcribe']['id'];
		
		if( !empty($_SESSION['transcribe']['uri_override']) )
		{
			$lang_id = $_SESSION['transcribe']['uri_override'];
			unset($_SESSION['transcribe']['uri_override']);
		}
			
		
		$language_abbr = $this->EE->input->get('lang');
		$language_abbr = empty($language_abbr) ? $this->EE->input->post('lang', TRUE) : $language_abbr;

		if(empty($language_abbr) && $return == 1)
		{
			$language_abbr = $this->language_abbreviation();
		}

		// structure URL switcher.
		if( $this->EE->db->table_exists('structure') AND !empty($language_abbr) )
		{
			// can't cache this one since it's only triggered when changing language... and not cached prior to here when thats the case

			$this->EE->db->select('id');
			$this->EE->db->from('transcribe_languages');
			$this->EE->db->where('abbreviation', $language_abbr);

			$language_id = $this->EE->db->get();
			$language_id = $language_id->row('id');	

			$this->sql = new Sql_structure();
			$site_pages = $this->sql->get_site_pages();

			$base_url = $this->EE->functions->fetch_site_index(0);

			// get current URL of page
			$current_segment = isset($this->EE->session->tracker[0]) ? $this->EE->session->tracker[0] : (isset($_SESSION['HTTP_REFERER']) ? $_SESSION['HTTP_REFERER'] : 'index');
			if($current_segment{0} != '/')
				$current_segment = '/'.$current_segment;

			$reverse = strrev( $current_segment );
			if($reverse{0} != '/')
				$current_segment .= '/';

			// array I need to search = $site_pages['uris']
			if( $current_segment == '/index/')
				$current_segment = '/';

			// added untested.. might throw error JR... please check
			if( class_exists('Sql_structure'))
			{ 

				// get all entry ids that match the slug
				foreach( $site_pages['uris'] as $entry_id => $slug)
				{
					$reverse = strrev( $slug );
					if($reverse{0} != '/')
						$slug .= '/';
					
					if( $current_segment == $slug )
					{
						$entry_ids[] = $entry_id;
					}
				}
				unset($entry_id);
			}

			if( !empty($entry_ids) )
			{
				$entry_ids = implode(",", $entry_ids);
				$sql = 'SELECT entry_id, language_id FROM `' . $this->EE->db->dbprefix('transcribe_entries_languages') . '` WHERE relationship_id = (SELECT relationship_id FROM `' . $this->EE->db->dbprefix('transcribe_entries_languages') . '` WHERE entry_id IN('.$entry_ids.') GROUP BY `relationship_id` ) AND language_id = '.$language_id.'';

				$relationships = $this->EE->db->query($sql);

				if($relationships->num_rows() >=1)
				{
					foreach($relationships->result_array() as $row)
					{
						if( $row['language_id'] == $language_id )
						{
							$new_page_entry_id = $relationships->row('entry_id');

							if( !empty($site_pages['uris'][$new_page_entry_id]) )
							{
								$redirect_url = $site_pages['uris'][$new_page_entry_id];

								if($return == 1)
								{
									return $redirect_url;
								}

								$redirect_url = $this->EE->functions->fetch_site_index() . $redirect_url;
								$redirect_url = $this->EE->functions->remove_double_slashes($redirect_url);
								$this->EE->functions->redirect($redirect_url);
							}
						}
					}
					// we have a page

				}
			}
		}

		if( ! Cache::get(array('lookup','route', $lang_id)) )
		{
			$sql = array();

			// Routes for template_group/template_name
			$sql[] = "SELECT";
			$sql[] = "CONCAT_WS('/', ttgl.content, ttl.content) as route,";
			$sql[] = "CONCAT_WS('/', ttgl2.content, ttl2.content) as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl2 ON ttgl2.template_group_id = tg.group_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl2 ON ttl2.template_id = t.template_id AND ttl2.language_id = ttgl2.language_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;
			$sql[] = "AND ttgl2.language_id = " . $lang_id;

			$sql[] = "UNION ALL";

			// Routes for template_group
			$sql[] = "SELECT";
			$sql[] = "ttgl.content as route,";
			$sql[] = "ttgl2.content as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl2 ON ttgl2.template_group_id = tg.group_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl2 ON ttl2.template_id = t.template_id AND ttl2.language_id = ttgl2.language_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;
			$sql[] = "AND t.template_name = 'index'";
			$sql[] = "AND ttgl2.language_id = " . $lang_id;
			$sql[] = "GROUP BY ttgl.content";

			$sql[] = "UNION ALL";

			// Routes for template_group (default)
			$sql[] = "SELECT";
			$sql[] = "ttgl.content as route,";
			$sql[] = "ttgl2.content as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl2 ON ttgl2.template_group_id = tg.group_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl2 ON ttl2.template_id = t.template_id AND ttl2.language_id = ttgl2.language_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;
			$sql[] = "AND tg.is_site_default = 'y'";
			$sql[] = "AND t.template_name = 'index'";
			$sql[] = "AND ttgl2.language_id = " . $lang_id;
			$sql[] = "GROUP BY ttgl.content";

			$sql[] = "UNION ALL";

			// Routes for template_name (default)
			$sql[] = "SELECT";
			$sql[] = "ttl.content as route,";
			$sql[] = "ttl2.content as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('template_groups') . " as tg";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('templates') . " as t ON t.group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl ON ttgl.template_group_id = tg.group_id";
			$sql[] = "JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl ON ttl.template_id = t.template_id AND ttl.language_id = ttgl.language_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_template_groups_languages') . " as ttgl2 ON ttgl2.template_group_id = tg.group_id";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_templates_languages') . " as ttl2 ON ttl2.template_id = t.template_id AND ttl2.language_id = ttgl2.language_id";
			$sql[] = "WHERE tg.site_id = " . $this->site_id . " AND t.site_id = " . $this->site_id;
			$sql[] = "AND tg.is_site_default = 'y'";
			$sql[] = "AND ttgl2.language_id = " . $lang_id;

			$route_lookup = $this->EE->db->query(implode(' ', $sql))->result();
			Cache::set(array('lookup','route', $lang_id), $route_lookup);
		}
		else
		{
			$route_lookup = Cache::get(array('lookup','route', $lang_id));
		}

		$route = array();
		$uri_string = trim($uri_string, '/');
		$segments = explode('/', $uri_string);

		// if the last page was 'index' remove it from segments so the homepage
		// doesn't have it upon switching languages
		if( count($segments) == 1 AND $segments[0] == 'index' )
			unset($segments[0]);

		// check route for segment_1/segment_2
		if( !empty($segments) AND !empty($segments[1]) )
		{
			foreach( $route_lookup as $row )
			{
				if( trim($row->route) == implode('/', array($segments[0], $segments[1])) )
				{
					$route = explode('/', trim($row->relationship));
					$segments[0] = $route[0];
					$segments[1] = $route[1];
					break;
				}
			}
		}

		// check route for segment_1
		if( empty($route) AND !empty($segments) )
		{
			foreach( $route_lookup as $row )
			{
				if( trim($row->route) == $segments[0] )
				{
					$segments[0] = trim($row->relationship);
					break;
				}
			}
		}

		// check route for all other segments
		if( ! Cache::get(array('lookup','entry', $lang_id)) )
		{
			$sql = array();

			$sql[] = "SELECT";
			$sql[] = "cr.url_title as route,";
			$sql[] = "cr2.url_title as relationship";
			$sql[] = "FROM " . $this->EE->db->dbprefix('transcribe_entries_languages') . " as tel";
			$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_entries_languages') . " as tel2 ON tel2.relationship_id = tel.relationship_id";
			$sql[] = "LEFT JOIN " . $this->EE->db->dbprefix('channel_titles') . " as cr ON cr.entry_id = tel.entry_id";
			$sql[] = "LEFT JOIN " . $this->EE->db->dbprefix('channel_titles') . " as cr2 ON cr2.entry_id = tel2.entry_id";
			$sql[] = "WHERE tel2.language_id = " . $lang_id;
			$sql[] = "AND cr.site_id = " . $this->site_id;

			$entry_lookup = $this->EE->db->query(implode(' ', $sql))->result();
			Cache::set(array('lookup','entry'), $entry_lookup);
		}
		else
		{
			$entry_lookup = Cache::get(array('lookup','entry'));
		}

		foreach( $segments as $key => $segment )
		{
			foreach( $entry_lookup as $row )
			{
				if( trim($row->route) == $segment )
					$segments[$key] = $row->relationship;
			}
		}
		return implode('/', $segments);
	}

	/**
	 * Return the peoper relationship for the route provided.
	 */
	public function _template_for_route( $uri_string )
	{
		$this->_get_routes();

		foreach( $this->routes as $route )
		{
			if( $route->route == $uri_string )
			{
				return $route->relationship;
			}
		}

		return FALSE;
	}

	// function to grab the language id's for a given list of entry_ids
	public function _get_language($entry_ids)
	{
		$languages = array();
		
		if(!empty($entry_ids))
		{
			$this->EE->db->where_in('entry_id', $entry_ids);
			$languages = $this->EE->db->get('transcribe_entries_languages');
			$languages = $languages->result_array();
		}

		return $languages;
	}

	public function _fix_structure_site_pages()
	{
		if( ! $this->EE->db->table_exists('structure') ) return FALSE;

		$site_pages = $this->EE->config->item('site_pages');
	

		// don't need to cache these query since it should only be run once.

		$uri_language_ids = $this->EE->db
				->select('ct.entry_id, ct.channel_id, tel.language_id, ct.url_title')
				->from('channel_titles as ct')
				->join('transcribe_entries_languages as tel', 'tel.entry_id = ct.entry_id')
				->join('structure_channels as sc', 'sc.channel_id = ct.channel_id')
				->where(array(
					'sc.type' => 'listing',
				))->get();


		$uri_language_ids = $uri_language_ids->result_array();

		// put results in array for look up later
		foreach($uri_language_ids as $row)
			$language_ids[$row['url_title']] = $row['language_id'];
			
		
		// relationship
		$sql = array();
		$sql[] = "SELECT";
		$sql[] = "cr.url_title as route,";
		$sql[] = "cr2.url_title as relationship";
		$sql[] = "FROM " . $this->EE->db->dbprefix('transcribe_entries_languages') . " as tel";
		$sql[] = "INNER JOIN " . $this->EE->db->dbprefix('transcribe_entries_languages') . " as tel2 ON tel2.relationship_id = tel.relationship_id";
		$sql[] = "LEFT JOIN " . $this->EE->db->dbprefix('channel_titles') . " as cr ON cr.entry_id = tel.entry_id";
		$sql[] = "LEFT JOIN " . $this->EE->db->dbprefix('channel_titles') . " as cr2 ON cr2.entry_id = tel2.entry_id";
		$sql[] = "WHERE cr.site_id = " . $this->site_id;

		
		$segment_data = $this->EE->db->query(implode(' ', $sql))->result_array();
		foreach($segment_data as $row)
			$segments_lookup['route'] = $row;


		foreach( $site_pages[$this->site_id]['uris'] as $key => $uri )
		{
			$segments = array_reverse(explode('/', trim($uri,'/')));
			$segments_count = count($segments);

			if( $segments_count > 1 )
			{
				if( ! empty($language_ids[$segments[0]]) )
				{
					for( $i=1; $i<$segments_count; $i++ )
					{
						foreach($segments_lookup as $url_title => $row)
						{
							if($url_title == $segments[$i] && $row['language_id'] == $language_ids[$segments[0]])
								$segment_lookup = $row['relationship'];
						}

						if( ! empty($segment_lookup) )
							$segments[$i] = $segment_lookup;
					}
				}
			}
			

			$segments = array_reverse($segments);

			if ( $this->EE->config->item('app_version') < '2.5.1')
				$site_pages[$this->site_id]['uris'][$key] = '/' . implode('/', $segments) . '/';
			else
				$site_pages[$this->site_id]['uris'][$key] = '/' . implode('/', $segments);
			
			
		}
		
		$this->EE->config->set_item('site_pages', $site_pages);
	}

	public function _save_cookie( $data )
	{
		$expires = 60*60*24*7; // 7 days
		$data = in_array(gettype($data), array('array','object')) ? serialize($data) : $data;

		//check to see if the cookie concent module is installed
		if ( $this->EE->config->item('app_version') >= '2.5')
		{
			if($this->EE->input->cookie('cookies_allowed') == 'y')
			{
				$this->EE->functions->set_cookie('transcribe', $data, $expires);
			}
		}
		else
		{
			$this->EE->functions->set_cookie('transcribe', $data, $expires);
			
		}
				

	}

	public function _get_cookie()
	{
		$cookie = $this->EE->input->cookie('transcribe');
		return @unserialize($cookie) ? unserialize($cookie) : $cookie;
	}

	public function _get_settings()
	{
		$settings = Cache::get(array('transcribe_settings'));

		if(empty($settings))
		{
			$settings = $this->EE->db->get_where('transcribe_settings', array('site_id' => $this->site_id), 1)->row();
			Cache::set(array('transcribe_settings'), $settings);
		}
		
		return $settings;
	}

	/*
	 * This function will go ahead and set the base site url based on if we have a language segment being injected or not.  It will go ahead and return it
	 */
	public function _set_base($basepath)
	{
		$language_abbreviation = $this->language_abbreviation();

		$language = Cache::get(array('current_lang', $language_abbreviation));
		
		if(empty($language))
		{
			$language = $this->EE->db->get_where('transcribe_languages', array('abbreviation ' => $language_abbreviation));
			$language = $language->row();

			Cache::set(array('current_lang', $language->abbreviation), $language);
		}

		$settings = $this->_get_settings();
		if( !empty($settings->force_prefix) )
		{
			if( $settings->force_prefix == 1 || ($settings->force_prefix == 2 && !empty($language->force_prefix)) )
			{
				// remove SELF constant from site_index (if found). re-add it later if it exists.
				$index_has_self = FALSE;
				if( strpos($basepath, SELF) !== FALSE )
				{
					$index_has_self = TRUE;
					// we might want to change the $this->EE->config->config['site_index'] to be $basepath
					$basepath = str_replace(SELF, '', $this->EE->config->config['site_index']);
				}

				// remove any current abbreviations
				$basepath = $this->_remove_abbreviation($basepath);

				$site_index = ($index_has_self ? SELF . '/' : '') . $language_abbreviation . '/';
				$basepath = $this->EE->functions->remove_double_slashes($site_index);
			}
		}
		return $basepath;
	}
	
	function array_contains($input, $search_value, $strict = false)
	{
		
		$tmpkeys = array();
		$input = array_flip($input);
		$keys = array_keys($input);

		foreach ($keys as $k)
		{
			if ($strict && strpos($k, $search_value) !== FALSE)
				$tmpkeys[] = $k;
			elseif (!$strict && stripos($k, $search_value) !== FALSE)
				$tmpkeys[] = $k;
		}

		return $tmpkeys;
	}
}

/* End of File: mod.transcribe.php */
